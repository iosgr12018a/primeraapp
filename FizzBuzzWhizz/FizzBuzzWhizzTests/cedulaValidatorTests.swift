//
//  cedulaValidatorTests.swift
//  FizzBuzzWhizzTests
//
//  Created by Sebastian Guerrero F on 4/25/18.
//  Copyright © 2018 Sebastian Guerrero. All rights reserved.
//

import XCTest
@testable import FizzBuzzWhizz


class cedulaValidatorTests: XCTestCase {
  let cedula = Cedula()
  override func setUp() {
    super.setUp()
    // Put setup code here. This method is called before the invocation of each test method in the class.
  }
  
  override func tearDown() {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    super.tearDown()
  }
  
  
  func testShouldReturnFalseIfLenghtIsDifferentThanTen() {
    XCTAssert(!cedula.validate("1234345"))
  }
  
  func testShouldResturnFalseIfContainsSomethingDifferentThanInts() {
    XCTAssert(!cedula.validate("1ab4345903"))
  }
  
  func testShouldResturnFalseIfTwoFirstCharacterArLowerThanOne() {
   XCTAssert(!cedula.validate("0012345678"))
  }
  
  func testShouldResturnFalseIfTwoFirstCharacterArGreaterThan24() {
    XCTAssert(!cedula.validate("2712345678"))
  }
  
  func testShouldResturnFalseIfThirdCharacterIsGreaterThan6() {
    XCTAssert(!cedula.validate("2172345678"))
  }
  
}
