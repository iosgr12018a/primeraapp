//
//  FizzBuzzWhizzTests.swift
//  FizzBuzzWhizzTests
//
//  Created by Sebastian Guerrero F on 4/24/18.
//  Copyright © 2018 Sebastian Guerrero. All rights reserved.
//

import XCTest
@testable import FizzBuzzWhizz

class FizzBuzzWhizzTests: XCTestCase {
  
  let juego = Juego()
  
  override func setUp() {
    super.setUp()
    // Put setup code here. This method is called before the invocation of each test method in the class.
  }
  
  override func tearDown() {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    super.tearDown()
  }
  
  func testShouldReturnAnArray() {
    let myArray = try! juego.fizzBuzzWhizz(start:1, length:100)
    XCTAssert(type(of: myArray) == type(of: []), "Must return an Array")
  }
  
  func testShoulReturnArrayFromStartWithLength() {
    let myArray = try! juego.fizzBuzzWhizz(start:33, length:78)
    XCTAssert(myArray.count == 78, "Must have 78 elements")
  }
  
  func testShoulReturnRightValueForIndex() {
    let myArray = try! juego.fizzBuzzWhizz(start:1, length:100)
    XCTAssert(myArray[0] as? Int ?? 0 == 1, "Must return 1")
    XCTAssert(myArray[13] as? Int ?? 0 == 14, "Must 14")
    XCTAssert(myArray[57] as? Int ?? 0 == 58, "Must 58")
  }
  
  func testShoulReturnFizzWhizzForThree() {
    let myArray = try! juego.fizzBuzzWhizz(start:1, length:100)
    XCTAssert(myArray[2] as? String ?? "" == "FizzWhizz", "Must return FizzWhizz")
  }
  
  func testShoulReturnFizzBuzzForFive() {
    let myArray = try! juego.fizzBuzzWhizz(start:1, length:100)
    XCTAssert(myArray[4] as? String ?? "" == "BuzzWhizz", "Must return BuzzWhizz")
  }
  
  func testShouldReturnFizzForMultipleOfThree() {
    let myArray = try! juego.fizzBuzzWhizz(start:1, length:100)
    XCTAssert(myArray[5] as? String ?? "" == "Fizz", "Must return Fizz")
    XCTAssert(myArray[50] as? String ?? "" == "Fizz", "Must return Fizz")
    XCTAssert(myArray[98] as? String ?? "" == "Fizz", "Must return Fizz")
  }
  
  func testShouldReturBuzzForMultipleOfFive() {
    let myArray = try! juego.fizzBuzzWhizz(start:1, length:100)
    XCTAssert(myArray[49] as? String ?? "" == "Buzz", "Must return Buzz")
    XCTAssert(myArray[24] as? String ?? "" == "Buzz", "Must return Buzz")
  }
  
  func testShouldReturFizzBuzzForMultipleOfFiveAndThree() {
    let myArray = try! juego.fizzBuzzWhizz(start:1, length:100)
    XCTAssert(myArray[14] as? String ?? "" == "FizzBuzz", "Must return FizzBuzz")
    XCTAssert(myArray[29] as? String ?? "" == "FizzBuzz", "Must return FizzBuzz")
    XCTAssert(myArray[89] as? String ?? "" == "FizzBuzz", "Must return FizzBuzz")
  }
  
  func testShouldReturWhizzForPrimeNumber() {
    let myArray = try! juego.fizzBuzzWhizz(start:1, length:100)
    XCTAssert(myArray[16] as? String ?? "" == "Whizz", "Must return Whizz")
    XCTAssert(myArray[6] as? String ?? "" == "Whizz", "Must return Whizz")
    XCTAssert(myArray[12] as? String ?? "" == "Whizz", "Must return Whizz")
  }
  
  func testShouldThrowErroForLenghtLowerThanOne() {
    XCTAssertThrowsError(try juego.fizzBuzzWhizz(start: 1, length: 0))
    XCTAssertThrowsError(try juego.fizzBuzzWhizz(start: 1, length: -1))

  }
}
