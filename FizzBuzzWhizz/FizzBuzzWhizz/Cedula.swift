//
//  Cedula.swift
//  FizzBuzzWhizz
//
//  Created by Sebastian Guerrero F on 4/25/18.
//  Copyright © 2018 Sebastian Guerrero. All rights reserved.
//

import Foundation

struct Cedula {
  
  func validate(_ ci:String) -> Bool {
    if ci.count != 10 {
      return false
    }
    
    guard let _ = Int(ci) else {
      return false
    }
    
    let range = ci.index(ci.startIndex, offsetBy: 1)
    
    let twoFirst = Int(String(ci[...range]))!
    if twoFirst < 1 || twoFirst > 24 {
      return false
    }
    
    if Int(ci[index])! > 6 {
      return false
    }
    return true
  }
  
}
