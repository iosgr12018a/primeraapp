
//
//  Juego.swift
//  FizzBuzzWhizz
//
//  Created by Sebastian Guerrero F on 4/24/18.
//  Copyright © 2018 Sebastian Guerrero. All rights reserved.
//

import Foundation

enum FizzBuzzWhizzError: Error {
  case InvalidLenght
}

struct Juego {
  func fizzBuzzWhizz(start: Int, length: Int) throws -> [Any]{
    if length < 1 {
      throw FizzBuzzWhizzError.InvalidLenght
    }
    let end = start + length - 1
    let array:[Any] = Array(start...end).map { (value) in
      if value == 3 {
        return "FizzWhizz"
      }
      if value == 5 {
        return "BuzzWhizz"
      }
      if value % 15 == 0 {
        return "FizzBuzz"
      }
      if value % 3 == 0 {
        return "Fizz"
      }
      if value % 5 == 0 {
        return "Buzz"
      }
      if isPrime(number: value) {
        return "Whizz"
      }
      return value
    }
    return array
  }
  
  func isPrime(number: Int) -> Bool {
    switch number {
    case 0, 1:
      return false
    case 2, 3:
      return true
    default:
      for i in 2...Int(sqrt(Double(number))) {
        if number % i == 0 {
          return false
        }
      }
      return true
    }
  }
}

