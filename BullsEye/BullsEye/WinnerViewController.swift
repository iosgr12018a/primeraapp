//
//  WinnerViewController.swift
//  BullsEye
//
//  Created by Sebastian Guerrero F on 5/2/18.
//  Copyright © 2018 Sebastian Guerrero. All rights reserved.
//

import UIKit

class WinnerViewController: UIViewController {
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Do any additional setup after loading the view.
  }
  
  @IBAction func ExitButtonPressed(_ sender: Any) {
    
    dismiss(animated: true, completion: nil)
  }
  
  
}
