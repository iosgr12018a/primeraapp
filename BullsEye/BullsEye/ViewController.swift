//
//  ViewController.swift
//  BullsEye
//
//  Created by Sebastian Guerrero F on 4/24/18.
//  Copyright © 2018 Sebastian Guerrero. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  
  @IBOutlet weak var targetLabel: UILabel!
  @IBOutlet weak var scoreLabel: UILabel!
  @IBOutlet weak var roundLabel: UILabel!
  @IBOutlet weak var gameSlider: UISlider!
  
  let gameModel = Game()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    gameModel.restartGame()
    setValues()
  }
  
  @IBAction func playButtonPressed(_ sender: Any) {
    let sliderValue = Int(gameSlider.value)
    gameModel.play(sliderValue: sliderValue)
    setValues()
  }
  
  @IBAction func restartButtonPressed(_ sender: Any) {
    gameModel.restartGame()
    setValues()
  }
  
  @IBAction func infoButtonPressed(_ sender: Any) {
  }
  
  @IBAction func WinnerButtonPressed(_ sender: Any) {
    if gameModel.isWinner {
      performSegue(withIdentifier: "toWinnerSegue", sender: self)
    }
  }
  
  func setValues() {
    targetLabel.text = "\(gameModel.target)"
    scoreLabel.text = "\(gameModel.score)"
    roundLabel.text = "\(gameModel.roundGame)"
  }
  
}

